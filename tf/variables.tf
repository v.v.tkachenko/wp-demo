variable "aws_access_key" {
    default = "AKIAQLSVMY4D6BSHWBOY"
}
variable "aws_secret_key" {
    default = "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"
}
variable "aws_key_path" {
    default = "/home/"
}
variable "aws_key_name" {
    default = "ansible"
}

resource "aws_key_pair" "ansible" {
  key_name = "ansible"
  public_key = "${file("/home/ansible.pub")}"
}

variable "aws_region" {
    description = "EC2 Region "
    default = "eu-west-1"
}

variable "amis" {
    description = "AMI"
    default = {
        eu-west-1 = "ami-07683a44e80cd32c5"
    }
}

variable "vpc_cidr" {
    description = "CIDR for all VPC"
    default = "10.0.0.0/16"
}

variable "public_subnet_cidr" {
    description = "CIDR for the Public Subnet"
    default = "10.0.0.0/24"
}

variable "private_subnet_cidr" {
    description = "CIDR for the Private Subnet"
    default = "10.0.1.0/24"
}
